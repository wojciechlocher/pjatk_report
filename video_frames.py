import cv2
import os

vidcap = cv2.VideoCapture(r'E:\airsim\segmentation_models.pytorch\examples\data\video\video2.avi')
success, image = vidcap.read()
count = 1300

path = 'E:/coco-annotator/datasets/pjatk_lab_2'
while success:
    cv2.imwrite(os.path.join(path, "dron_lab_%d.png" % count), image)
    success,image = vidcap.read()
    print('Read a new frame: ', success)
    count += 1
